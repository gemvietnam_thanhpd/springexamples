package com.springapp.autowire;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by PHANTHANH on 8/7/2015.
 */
public class Student {

    private String name;
    private Address address;


    @Autowired(required = false)
    public Student(Address address) {
        this.address = address;

    }

    public void foo(){
        System.out.println("Foo...");
    }










}
