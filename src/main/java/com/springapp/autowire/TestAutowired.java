package com.springapp.autowire;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by PHANTHANH on 8/7/2015.
 */
public class TestAutowired {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("autwired-config.xml");

        Student student = (Student)context.getBean("student");

        student.foo();
    }
}
