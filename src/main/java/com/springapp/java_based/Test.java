package com.springapp.java_based;

import com.springapp.di.TextEditor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by PHANTHANH on 8/5/2015.
 */
public class Test {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.register(TextEditor.class);
        context.register(TextEditorConfig.class);
        context.refresh();

        HelloWorld helloWorld = (HelloWorld)context.getBean(HelloWorld.class);
        helloWorld.setMessage1("1df");
        helloWorld.getMessage1();

        TextEditor textEditor = context.getBean(TextEditor.class);
        textEditor.spellCheck();

    }




}
