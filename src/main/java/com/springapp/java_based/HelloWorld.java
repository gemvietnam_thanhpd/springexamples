package com.springapp.java_based;

import org.springframework.context.annotation.Configuration;

/**
 * Created by PHANTHANH on 8/5/2015.
 */


public class HelloWorld {

    private String message1;
    private String message2;


    public void setMessage1(String message1) {
        this.message1 = message1;
    }

    public void setMessage2(String message2) {
        this.message2 = message2;
    }


    public String getMessage1() {
        System.out.println("World Message: "+ message1);
        return message1;
    }

    public String getMessage2() {
        System.out.println("World Message: "+ message2);
        return message2;
    }

    public void init() {
        System.out.println("Init");
    }

    public void destroy() {
        System.out.println("Destroyed");
    }
}
