package com.springapp.java_based;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by PHANTHANH on 8/5/2015.
 */

@Configuration
public class HelloworldConfig {

    @Bean
    public HelloWorld helloWorld(){
        return new HelloWorld();
    }

}
