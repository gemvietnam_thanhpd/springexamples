package com.springapp.java_based;

import com.springapp.di.SpellChecker;
import com.springapp.di.TextEditor;

/**
 * Created by PHANTHANH on 8/5/2015.
 */
public class TextEditorConfig {

    public TextEditor textEditor(){
        return new TextEditor(spellChecker());
    }

    public SpellChecker spellChecker(){
        return new SpellChecker();
    }

}
