package com.springapp.mvc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

/**
 * Created by PHANTHANH on 8/5/2015.
 */
public class MainApp {
    public static void main(String[] args) {
        AbstractApplicationContext context =
                new ClassPathXmlApplicationContext("Beans.xml");
        HelloWorld helloWorld = (HelloWorld) context.getBean("helloWorld");

        HelloIndia helloIndia = (HelloIndia) context.getBean("helloIndia");
        helloIndia.getMessage2();

       /* HelloWorld objectB = (HelloWorld) context.getBean("myHello");
        objectB.getMessage();*/
        context.registerShutdownHook();
    }
}
