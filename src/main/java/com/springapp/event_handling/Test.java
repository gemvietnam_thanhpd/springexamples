package com.springapp.event_handling;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by PHANTHANH on 8/5/2015.
 */
public class Test {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("event-config.xml");

        context.start();

        HelloWorld helloWorld = (HelloWorld)context.getBean("helloWorld");

        System.out.println(helloWorld.getMessage());

        context.stop();
    }



}
