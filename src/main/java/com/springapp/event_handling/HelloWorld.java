package com.springapp.event_handling;

/**
 * Created by PHANTHANH on 8/5/2015.
 */
public class HelloWorld {

    private String message;

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
