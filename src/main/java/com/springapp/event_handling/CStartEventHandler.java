package com.springapp.event_handling;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ApplicationContextEvent;
import org.springframework.context.event.ContextStartedEvent;

/**
 * Created by PHANTHANH on 8/5/2015.
 */
public class CStartEventHandler implements ApplicationListener<ContextStartedEvent>{


    @Override
    public void onApplicationEvent(ContextStartedEvent contextStartedEvent) {
        System.out.println("ContextStartedEvent Received");
    }
}
