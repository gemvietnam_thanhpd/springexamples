package com.springapp.namealias;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by PHANTHANH on 8/7/2015.
 */
public class TestNameAlias {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("namealias-config.xml");

        Student student = (Student) context.getBean("student");
        student.sayHello();
        student.setName("Another name");
        Student children = (Student) context.getBean("children");
        children.sayHello();

    }

}
