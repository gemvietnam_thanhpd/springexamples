package com.springapp.annotation.qualifier;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by PHANTHANH on 8/5/2015.
 */
public class Test {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("profile-config.xml");

        Profile profile = (Profile)context.getBean("profile");
     //   profile.printAge();
        profile.printName();
    }
}
