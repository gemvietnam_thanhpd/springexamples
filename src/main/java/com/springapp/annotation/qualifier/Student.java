package com.springapp.annotation.qualifier;

import org.springframework.beans.factory.annotation.Required;

/**
 * Created by PHANTHANH on 8/5/2015.
 */
public class Student {
    private Integer age;
    private String name;

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getAge() {
        return age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
