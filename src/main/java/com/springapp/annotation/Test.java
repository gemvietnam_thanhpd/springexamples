package com.springapp.annotation;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by PHANTHANH on 8/5/2015.
 */
public class Test {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("student-config.xml");
        Student student = (Student)context.getBean("student");
    }
}
