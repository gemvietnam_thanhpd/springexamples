package com.springapp.automaticwire;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

/**
 * Created by PHANTHANH on 8/7/2015.
 */

@Component
public class CDPlayer {
    private CompactDisc cd;

    public CDPlayer(){

    }

    public CDPlayer(CompactDisc cd) {
        this.cd = cd;
    }

    @Autowired(required = false)
    public void setCd(CompactDisc cd) {
        this.cd = cd;
    }

    public void play() {
        cd.play();
    }

}
