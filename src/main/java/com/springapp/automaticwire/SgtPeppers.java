package com.springapp.automaticwire;

import org.springframework.stereotype.Component;

import java.util.Comparator;

/**
 * Created by PHANTHANH on 8/7/2015.
 */

@Component
public class SgtPeppers implements CompactDisc {
    private String title = "Sgt. Pepper's Lonely Hearts Club Band";
    private String artist = "The Beatles";

    @Override
    public void play() {
        System.out.println("Playing " + title + " by " + artist);

    }
}
