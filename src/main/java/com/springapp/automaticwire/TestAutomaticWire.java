package com.springapp.automaticwire;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by PHANTHANH on 8/7/2015.
 */
public class TestAutomaticWire {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.register(CDPlayerConfig.class);

        context.refresh();


     /*   CompactDisc compactDisc = (CompactDisc) context.getBean(SgtPeppers.class);

        CompactDisc another = (CompactDisc) context.getBean("sgtPeppers");
        another.play();*/

        System.out.println("\n\n");

        CDPlayer cdPlayer = (CDPlayer)context.getBean(CDPlayer.class);
        cdPlayer.play();


    }
}
