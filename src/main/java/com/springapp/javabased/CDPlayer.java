package com.springapp.javabased;

import com.springapp.automaticwire.CompactDisc;

/**
 * Created by PHANTHANH on 8/7/2015.
 */
public class CDPlayer {
    private CompactCD cd;

    public CDPlayer(CompactCD cd){
        this.cd = cd;
    }

    public void foo(){
        System.out.println("Foo...");
    }

}
