package com.springapp.javabased;

import com.springapp.automaticwire.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by PHANTHANH on 8/7/2015.
 */
@Configuration
public class CDConfig {

    @Bean(name = "cdPlayer") // default name se la ten class + lowercase
    public CDPlayer cdPlayer(){
        return new CDPlayer(compactCD()); // inject compactCD bean vao cdPlayer bean
    }
    @Bean
    public CompactCD compactCD(){
        return new CompactCD();
    }


}
