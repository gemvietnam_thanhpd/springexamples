package com.springapp.javabased;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by PHANTHANH on 8/7/2015.
 */
public class Test {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(CDConfig.class);
        CDPlayer cdPlayer = context.getBean(CDPlayer.class);

        cdPlayer.foo();
    }
}
