package com.springapp.di;

import javax.xml.soap.Text;

/**
 * Created by PHANTHANH on 8/5/2015.
 */
public class TextEditor {
    SpellChecker spellChecker;


    public TextEditor(SpellChecker spellChecker) {
        this.spellChecker = spellChecker;
    }

    public void setSpellChecker(SpellChecker spellChecker) {
        System.out.println("Inside setSpellChecker");
        this.spellChecker = spellChecker;
    }

    public SpellChecker getSpellChecker() {
        return spellChecker;
    }

    public void spellCheck() {
        spellChecker.checkSpelling();

    }

}
