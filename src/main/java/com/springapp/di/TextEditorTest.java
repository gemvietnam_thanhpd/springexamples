package com.springapp.di;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by PHANTHANH on 8/5/2015.
 */
public class TextEditorTest {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("TextEditor.xml");

        TextEditor te = (TextEditor) context.getBean("anotherTextEditor");
        te.spellCheck();

    }

}
