package com.springapp.collection;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by PHANTHANH on 8/5/2015.
 */
public class Test {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("Collection-config.xml");
        JavaCollection javaCollection = (JavaCollection)context.getBean("javaCollection");

        javaCollection.getAddressList();
        javaCollection.getAddressSet();
        javaCollection.getAddressMap();
        javaCollection.getAddressPro();
    }
}
