package com.springapp.collection;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * Created by PHANTHANH on 8/5/2015.
 */
public class JavaCollection {
    List addressList;
    Set addressSet;
    Map addressMap;
    Properties addressPro;

    public void setAddressList(List addressList) {
        this.addressList = addressList;
    }

    public void setAddressSet(Set addressSet) {
        this.addressSet = addressSet;
    }

    public void setAddressMap(Map addressMap) {
        this.addressMap = addressMap;
    }

    public void setAddressPro(Properties addressPro) {
        this.addressPro = addressPro;
    }

    public List getAddressList() {
        System.out.println("List elements: " + addressList);
        return addressList;
    }

    public Set getAddressSet() {
        System.out.println("Set elements: " + addressSet);
        return addressSet;
    }

    public Map getAddressMap() {
        System.out.println("Map elements: " + addressMap);
        return addressMap;
    }

    public Properties getAddressPro() {
        System.out.println("Prop elements: " + addressPro);
        return addressPro;
    }


}
