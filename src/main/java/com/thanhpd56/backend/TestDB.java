package com.thanhpd56.backend;

import com.springapp.collection.JavaCollection;
import com.springapp.di.SpellChecker;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * Created by PHANTHANH on 8/11/2015.
 */
public class TestDB {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("jdbc-config.xml");



        StudentJDBCTemplate jdbcTemplate = context.getBean("studentJDBCTemplate", StudentJDBCTemplate.class);

        System.out.println("------Add students to db -------------");
        jdbcTemplate.create("Nam", 12);
        jdbcTemplate.create("Hoang", 12);

        System.out.println("------Listing Multiple Records--------" );

        List<Student> studentList = jdbcTemplate.listStudents();
        for(Student student : studentList){
            System.out.println(String.valueOf(student.getId()) + " - " + student.getName() + ": " + student.getAge());
        }


    }

}
