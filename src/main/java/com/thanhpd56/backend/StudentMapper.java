package com.thanhpd56.backend;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by PHANTHANH on 8/11/2015.
 */
public class StudentMapper implements RowMapper<Student> {
    @Override
    public Student mapRow(ResultSet resultSet, int i) throws SQLException {

        Student student = new Student();
        student.setName(resultSet.getString("name"));
        student.setId(resultSet.getInt("id"));
        student.setAge(resultSet.getInt("age"));
        return student;
    }
}
