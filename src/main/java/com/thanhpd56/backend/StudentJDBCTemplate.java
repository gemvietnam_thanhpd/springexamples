package com.thanhpd56.backend;

import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.List;

/**
 * Created by PHANTHANH on 8/11/2015.
 */
public class StudentJDBCTemplate implements StudentDAO {
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;

    @Override
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        jdbcTemplateObject = new JdbcTemplate(dataSource);

    }

    @Override
    public void create(String name, Integer age) {
        String query = "insert into student1(name, age) values(?, ?)";
        jdbcTemplateObject.update(query, name, age);
        return;
    }

    @Override
    public Student getStudent(Integer id) {
        String query = "select * from student1 where id=?";
        Student student = jdbcTemplateObject.queryForObject(query, new Object[]{id}, new StudentMapper());
        return student;
    }

    @Override
    public List<Student> listStudents() {

        String query = "select * from student1";
        List<Student> students = jdbcTemplateObject.query(query, new StudentMapper());
        return students;
    }

    @Override
    public void delete(Integer id) {
        String query = "delete from student1 where id = ?";
        jdbcTemplateObject.update(query, id);
    }

    @Override
    public void update(Integer id, Integer age) {
        String query = "update student1 set age=? where id=?";
        jdbcTemplateObject.update(query, age, id);
        System.out.println("Updated Record with ID = " + id);
        return;

    }
}
